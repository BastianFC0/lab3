//Bastian Fernandez Cortez
//2041556
package LinearAlgebra;

public class Test {
    public static void main(String[] args){
        Vector3d v = new Vector3d(1,2,3);
        Vector3d v2 = new Vector3d(1,2,3);
        Vector3d vnew = v.add(v2);
        System.out.println(vnew.getX()+" "+vnew.getY()+" "+vnew.getZ());
    }
}
