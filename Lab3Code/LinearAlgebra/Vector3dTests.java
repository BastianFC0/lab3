//Bastian Fernandez Cortez
//2041556
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class Vector3dTests {
    @Test
    public void testgetX(){
        Vector3d v = new Vector3d(1,2,3);
        assertEquals(1,v.getX());
    } 
    @Test
    public void testgetY(){
        Vector3d v = new Vector3d(1,2,3);
        assertEquals(2,v.getY());
    }
    @Test
    public void testgetZ(){
        Vector3d v = new Vector3d(1,2,3);
        assertEquals(3,v.getZ());
    } 
    @Test
    public void testMagnitude(){
        Vector3d v = new Vector3d(1,2,2);
        assertEquals(3,v.magnitude());
    }
    @Test
    public void testDotProduct(){
        Vector3d v = new Vector3d(1,2,2);
        Vector3d v1 = new Vector3d(1,2,2);
        assertEquals(9,v.dotProduct(v1));
    }
    @Test
    public void testAdd(){
        Vector3d v = new Vector3d(1,2,2);
        Vector3d v1 = new Vector3d(1,2,2);
        assertEquals(2,v.add(v1).getX());
        assertEquals(4,v.add(v1).getY());
        assertEquals(4,v.add(v1).getZ());
    }
}
